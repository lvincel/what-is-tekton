Tekton est un framework pour créer des système de CI/CD nativement sur Kubernetes. Il a débuté en aout 2018 chez [KNative](https://knative.dev/) et rejoins la [CDF](https://cd.foundation/) en février 2019 avec une release 0.1.0.
La dernière release à l'heure actuelle est 0.10.1.

qq docs: [Custom Resources](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/) & [développement des CustomeResources](https://kubernetes.io/docs/tasks/access-kubernetes-api/custom-resources/custom-resource-definitions/)

![alt text](resources/TektonPipeline_Arch.png "Tekton objects")

# __Task__
* référence une collection d'étapes représentant des actions à effectuer pour la CI/CD
* référence des entrants, sortants à ce groupe d'étape

## exemples

```yaml
apiVersion: tekton.dev/v1alpha1
kind: Task
metadata:
  name: echo-hello-world
spec:
  steps:
    - name: echo
      image: ubuntu
      command:
        - echo
      args:
        - "hello world"
```

# __TaskRun__
* référence une `Task`
* exécute la suite des étapes référencées dans la `Task` (la défénition d'une `Task` peut être embedded dans la `TaskRun`)

## exemples

```yaml
apiVersion: tekton.dev/v1alpha1
kind: TaskRun
metadata:
  name: echo-hello-world-task-run
spec:
  taskRef:
    name: echo-hello-world
```

# __PipelineResources__
* référence des objets entrants/sortant des `Task` (repo GitHub, image Docker, un jar)

## exemples

__git__

```yaml
apiVersion: tekton.dev/v1alpha1
kind: PipelineResource
metadata:
  name: wizzbang-git
  namespace: default
spec:
  type: git
  params:
    - name: url
      value: https://github.com/wizzbangcorp/wizzbang.git
    - name: revision
      value: master
```

__image__

```yaml
apiVersion: tekton.dev/v1alpha1
kind: PipelineResource
metadata:
  name: kritis-resources-image
  namespace: default
spec:
  type: image
  params:
    - name: url
      value: gcr.io/staging-images/kritis
```

__cluster__

```yaml
apiVersion: tekton.dev/v1alpha1
kind: PipelineResource
metadata:
  name: test-cluster
spec:
  type: cluster
  params:
    - name: url
      value: https://10.10.10.10 # url to the cluster master node
    - name: cadata
      value: LS0tLS1CRUdJTiBDRVJ.....
    - name: token
      value: ZXlKaGJHY2lPaU....
```

application in a Task
```yaml
apiVersion: tekton.dev/v1alpha1
kind: Task
metadata:
  name: deploy-image
  namespace: default
spec:
  inputs:
    resources:
      - name: workspace
        type: git
      - name: dockerimage
        type: image
      - name: testcluster
        type: cluster
  steps:
    - name: deploy
      image: image-with-kubectl
      command: ["bash"]
      args:
        - "-c"
        - kubectl --kubeconfig
          /workspace/$(inputs.resources.testcluster.name)/kubeconfig --context
          $(inputs.resources.testcluster.name) apply -f /workspace/service.yaml'
```

# __Pipeline__
* référence un graph de `Tasks`
* référence des `PipelineResources` qui seront utilisés en I/O des `Tasks`
* fournit un méchanisme d'ordenancement des `Tasks`

## exemples

```yaml
apiVersion: tekton.dev/v1alpha1
kind: Pipeline
metadata:
  name: pipeline-with-parameters
spec:
  params:
    - name: context
      type: string
      description: Path to context
      default: /some/where/or/other
  tasks:
    - name: build-skaffold-web
      taskRef:
        name: build-push
      params:
        - name: pathToDockerFile
          value: Dockerfile
        - name: pathToContext
          value: "$(params.context)"
```
  
# __PipelineRun__
* reference un `Pipeline` et lance un `TaskRun` pour chaque `Task`

## exemples

```yaml
apiVersion: tekton.dev/v1alpha1
kind: PipelineRun
metadata:
  name: pipelinerun-with-parameters
spec:
  pipelineRef:
    name: pipeline-with-parameters
  params:
    - name: "context"
      value: "/workspace/examples/microservices/leeroy-web"
```
